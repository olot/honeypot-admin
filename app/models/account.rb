class Account
  include Mongoid::Document
  #include trackable
  
  field :login_id, type: String
  field :username, type: String
  field :email, type: String
  field :type, type: String
  field :phone, type: String
  #has_many :maintenance_requests, inverse_of: :account

  scope :user, -> { where type: 'user' }
  scope :helper, -> { where type: 'helper' }

  
  
  rails_admin do
    list do
      scopes([:user, :helper, nil])
    end    
  end
  
end
