class BusinessBooking
  include Mongoid::Document
  include Mongoid::Paperclip
  include  Mongoid::Attributes::Dynamic 
  belongs_to :property
  belongs_to :business_helper
  #has_one :period_option
  has_one :period_option, :inverse_of => :business_booking
 
  field :total_due, type: Integer
  field :booking_date, type: Date
  field :booking_time, type: String
  field :cleaning_length, type: Float
  field :completed, type: Boolean
  field :notes, type: String
  
  scope :weekly, -> { where frequency: 'weekly'}
  scope :bi_weekly, -> { where frequency: 'bi-weekly'}
  scope :monthly, -> { where frequency: 'monthly'}
  scope :completed, -> { where completed: 'true'}

  #embeds_many :attachments, :cascade_callbacks => true
  #validates_attachment_content_type :attachment, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  attr_accessor :period_option_id

  #before_create :ensure_helper_hours
  
  
  def booking_time_enum
    ["7:00","7:30","8:00","8:30","9:00","9:30","10:00","10:30","11:00","11:30",
     "12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30","16:00",
     "16:30","17:00","17:30","18:00"].collect{|i| [i,i]}
    
  end
  
  def self.todays_bookings
    self.where({'booking_date' =>
                  {'$gte' => DateTime.now.beginning_of_day,
                   '$lte' => DateTime.now.end_of_day}})
  end

  def self.this_weeks
    self.where({'booking_date' =>
                {'$gte' => DateTime.now.beginning_of_week(:sunday),
                 '$lte' => DateTime.now.end_of_week(:sunday)}})
  end

  def self.this_months
    self.where({'booking_date' =>
                {'$gte' => DateTime.now.beginning_of_month,
                 '$lte' => DateTime.now.end_of_month}})
  end

  def display_booking_time
    Time.parse(booking_date.to_s + " " +  booking_time).strftime("%d/%m/%Y %H:%M")
  end
  
  def start_time
    self.booking_date
  end

  def cleaning_length_enum
    (3..8).step(0.5).to_a
  end
   
  rails_admin do
    list do
      scopes([:todays_bookings, :this_weeks, :this_months, :all])
      #filters([:period_option])
      field :display_booking_time do
        label do
          "Booking Date"
        end
      end
      field :cleaning_length
      field :total_due
      #include_all_fields
    end
    edit do      
      field :total_due
      field :period_option do
        inline_edit false
        label do
          "Frequency"
        end
      end
      field :booking_date
      field :booking_time
      field :cleaning_length
      field :property
      field :business_helper
      field :notes    
    end
  end
  
end
