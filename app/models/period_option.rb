class PeriodOption
  include Mongoid::Document
  field :title, type: String

  belongs_to :business_booking
  
  def name
    self.title
  end
  
end
