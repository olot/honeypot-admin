class ProfileImage
  include Mongoid::Document
  include Mongoid::Paperclip
  #validates_attachment_content_type :attachment, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  field :business_helper_id, type: String

  embedded_in :business_helper, :inverse_of => :profile_images

  has_mongoid_attached_file :attachment,
                            :path           => ':rails_root/storage/',
                            :storage        => :file,
                            :styles => {
                              :original => ['1920x1680>', :jpg],
                              :small    => ['100x100#',   :jpg],
                              :medium   => ['250x250',    :jpg],
                              :large    => ['500x500>',   :jpg]
                            },
                            :convert_options => { :all => '-background white -flatten +matte' }
  
end
