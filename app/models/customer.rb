class Customer
  include Mongoid::Document
  field :display_name, type: String
  #field :property_count, type: Integer
  has_many :properties
  #has_many :business_bookings, :through => :properties

  has_many :contracts

  def bookings
    self.properties.collect{|i| i.business_bookings}.flatten
  end
  
  def number_of_properties
    self.properties.count
  end
  
  def name
    self.display_name
  end
  
  rails_admin do
    edit do
      field :display_name do
        html_attributes rows: 1, cols: 50
      end
    end
    list do
      field :display_name
      field :number_of_properties
    end
  end
end
