class BusinessAdmin < Account
  include Mongoid::Document
  include ActiveModel::SecurePassword  
  has_secure_password
  #authenticates_with_sorcery!
  
  field :type, type: String, default: "business_admin"
  field :password_digest, type: String
end
