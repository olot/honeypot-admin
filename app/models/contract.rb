class Contract
  include Mongoid::Document
  field :business_helper_id, type: String
  field :customer_id, type: String
  field :type, type: Array
  
  belongs_to :business_helper
  belongs_to :customer

  def type_enum
    types = "freelancer, dispatch, outsourcing, limited hour dispatch, limited hour outsourcing, other".split(",")
    types.collect do |i|
     [i,i.parameterize.underscore.to_sym]
    end
  end
  
  
end
