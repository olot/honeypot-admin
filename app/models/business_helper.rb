class BusinessHelper < Account
  include Mongoid::Document
  include Mongoid::Paperclip
    
  field :type, type: String, default: "business_helper"
  field :helper_type, type: Array
  field :desired_max_hours, type: Float
  field :preferred_days, type: Array
  field :preferred_building_type, type: Array
  #field :contract_type, type: Array
  has_many :contracts 
  has_mongoid_attached_file :profile_image

  validates_attachment_content_type :profile_image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  def preferred_building_type_enum
    Property::BUILDING_TYPES.collect{|i| [i,i]}
  end
  
  def preferred_days_enum
    %w(Sunday Monday Tuesday Wednesday Thursday Friday Saturday).collect{|i| [i,i]}
  end
  
  def helper_type_enum
    [['Office Care', :office_care], ['Cleaner',:cleaner]]
  end  
  
  has_many :business_bookings
  
  def bookings
    self.business_bookings
  end
  
  def name
    self.username
  end

  def contract_count
    contracts.count
  end

  def booked_hours_this_month
    business_bookings.this_months.to_a.pluck(:cleaning_length).sum
  end

  def completed_hours_this_month
    business_bookings.this_months.completed.to_a.pluck(:cleaning_length).sum
  end

  def hours_booked
    [booked_hours_this_month,completed_hours_this_month].join("/")
  end
  
  rails_admin do
    list do
      field :login_id
      field :email
      field :phone
      field :contract_count
      field :desired_max_hours
      field :hours_booked do
        label do
          "Hours This Month (Done/All)"
        end
      end
    end
  end

  
end
