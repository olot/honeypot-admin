class Property
  include Mongoid::Document
  
  #include Trackable
  field :address, type: String
  field :room_count, type: Integer
  field :building_name, type: String
  field :building_type, type: Array

  BUILDING_TYPES = ["share house", "coworking space", "office", "retail", "other"]
  
  belongs_to :customer
  has_many :business_bookings

  def building_type_enum
    BUILDING_TYPES.collect{|i| [i,i]}
  end
  
  def bookings
    self.business_bookings
  end
  
  def name
    self.building_name
  end
  
  rails_admin do
    list do
      # Manually enable/disable per field
      field :name do
        filterable true
      end
      field :customer
      field :room_count
    end
    edit do
      field :customer
      field :address
      field :room_count
      field :building_name do
        html_attributes rows: 1, cols: 20
      end
    end
    # configure :client do
    # end
  end

  
end
