class AdminSessionsController < ApplicationController
  def new
    @admin = BusinessAdmin.new
  end

  #returns nil or admin object
  def create
    login_id = params[:login_id]
    password = parmams[:password]
    
    @admin = BusinessAdmin.where(login_id: login_id).first
    authenticated = @admin.try(:authenticate, password) ? @admin : nil
    reset_session if authenticated
    session[:current_user]  = authenticated ? authenticated : nil
  end

  def destroy
    reset_session
  end
end
