require Rails.root.join('lib', 'rails_admin', 'calendar.rb')
require Rails.root.join('lib', 'rails_admin', 'customer_calendar.rb')
require Rails.root.join('lib', 'rails_admin', 'day_view.rb')
require Rails.root.join('lib', 'rails_admin', 'property_calendar.rb')
require Rails.root.join('lib', 'rails_admin', 'todays_bookings.rb')
require Rails.root.join('lib', 'rails_admin', 'weekly_overview.rb')
require Rails.root.join('lib', 'rails_admin', 'monthly_overview.rb')
require Rails.root.join('lib', 'rails_admin', 'helper_contract.rb')
require Rails.root.join('lib', 'rails_admin', 'customer_contract.rb')
require Rails.root.join('lib', 'rails_admin', 'calendar_overview.rb')

#Dir["/lib/rails_admin/*.rb"].each {|file| require file }



RailsAdmin.config do |config|

  config.main_app_name = ["HoneyPot", "Back Office"]

  ### Popular gems integration

  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  config.authenticate_with do
    # Use sorcery's before filter to auth users
    #require_login
    
    #before filter rails_admin uses for auth
    #authenticate_or_request_with_http_basic('Login required') do |login_id, password|
    #  admin = BusinessAdmin.where(login_id:login_id).first
    #  admin.authenticate(password) if admin
    #end
  end
  
  config.current_user_method(&:current_user)
  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0
  config.audit_with :mongoid_audit
  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  Mongoid::History.modifier_class_name = 'BusinessAdmin'
  Mongoid::Userstamp.config do |c|
    c.user_reader = :current_account
    c.user_model = :business_admin
  end

 # config.navigation_static_label = "Bookings Overview"

 # config.navigation_static_links = {
 #   "Today's Bookings" => '/admin/',
 #   "Weekly Overview" => '/admin/business_helpers',
 #   "Monthly Overview" => '/admin/business_helpers'
 # }

  
  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  config.show_gravatar = true

  config.actions do
    #root :todays_bookings, :dashboard # subclass Dashboard. Accessible at /admin/my_dashboard
    #root :todays_bookings, :todays_bookings
    root :weekly_overview, :weekly_overview
    root :monthly_overview, :monthly_overview
    collection :bookings_calendar_overview, :calendar_overview  do
      only ['BusinessBooking']
    end
    index do
      except ['Contract']
    end
    new
    dashboard
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    customer_calendar
    property_calendar
    day do
    end
    ## With an audit adapter, you can add:
    history_index
    history_show
    helper_contract
    customer_contract
  end
end
