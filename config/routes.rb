Rails.application.routes.draw do
  get 'admin_sessions/new'
  get 'admin_sessions/create'
  get 'admin_sessions/destroy'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #root :to => "rails_admin/main#index"
  scope '/admin' do 
    get 'root' => 'rails_admin/main#index', :as => :root
    get 'login' => 'admin_sessions#new', :as => :login
    post 'logout' => 'admin_sessions#destroy', :as => :logout
  end
end
