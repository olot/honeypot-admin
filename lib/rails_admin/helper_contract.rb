module RailsAdmin
  module Config
    module Actions
      class HelperContract < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)
         register_instance_option :visible? do
          authorized? && bindings[:object].class == BusinessHelper
         end
        # We want the action on members, not the Users collection
        register_instance_option :member do
          true
        end
        register_instance_option :link_icon do
          'icon-list-alt'
        end
        register_instance_option :controller do
          proc do
            @helper = BusinessHelper.find(params["id"])
            render action: @action.template_name
          end
        end        
        # You may or may not want pjax for your action
        register_instance_option :pjax? do
          false
        end
      end
    end
  end
end
