module RailsAdmin
  module Config
    module Actions
      class CalendarOverview < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)
        register_instance_option :link_icon do
          'icon-calendar'
        end
        # You may or may not want pjax for your action

        register_instance_option :controller do
          proc do
            @bookings = BusinessBooking.all.to_a
            render action: @action.template_name
          end
        end
       
        register_instance_option :pjax? do
          false
        end
      end
    end
  end
end
